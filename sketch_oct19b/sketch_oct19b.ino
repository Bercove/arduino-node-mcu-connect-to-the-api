#include <SPI.h>
#include <MFRC522.h>
//NodeMCU--------------------------
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
//************************************************************************
#define SS_PIN  D2  //D2
#define RST_PIN D1  //D1
//************************************************************************
MFRC522 mfrc522(SS_PIN, RST_PIN); // Create MFRC522 instance.
//************************************************************************
/* Set these to your desired credentials. */
const char *ssid = "Lab 3";
const char *password = "123456789";
const char* device_token  = "2c4f3c61aa79d533";
//************************************************************************
String URL = "http://192.168.5.25:8025/api/v1/attendance/"; //computer IP or the server domain
String getData, Link, postData;
//************************************************************************
void setup() {
  delay(1000);
  Serial.begin(115200);
  SPI.begin();  // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522 card
  //---------------------------------------------
    connectToWiFi();
}
  //************************************************************************
void loop() {
  //check if there's a connection to Wi-Fi or not
  if(!WiFi.isConnected()){
    connectToWiFi();    //Retry to connect to Wi-Fi
  }

  String inStringDec = "";
  String inStringHex = "";
  String inStringHexRev = "";

  if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial())
  {
    for (byte i = 0; i < mfrc522.uid.size; i++) {
      Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
      Serial.print(mfrc522.uid.uidByte[i], HEX);
      inStringHex += String(mfrc522.uid.uidByte[i], HEX);
      inStringHexRev += String(mfrc522.uid.uidByte[mfrc522.uid.size - (i + 1)], HEX);
    }

    //convert to decimal UID
    String WorkingStringA = inStringHex;
    long unsigned A = strtol(WorkingStringA.c_str(),NULL,16);
    Serial.println();
    Serial.println(F("long unsigned A:"));
    Serial.print(A);
    WorkingStringA = String(A);
    String WorkingStringB = inStringHexRev;
    Serial.println();
    Serial.println(F("(NOT!)WorkingStringB:"));
    Serial.print(WorkingStringB);
    long unsigned B = strtoul(WorkingStringB.c_str(),NULL,16);
    Serial.println();
    Serial.println(F("long unsigned B:"));
    Serial.print(B);
    WorkingStringB = String(B);
    Serial.println();
    Serial.println(F("Hex normal and reverse:"));
    Serial.print(inStringHex);
    Serial.println();
    Serial.print(inStringHexRev);
    Serial.println();
    Serial.println(F("Decimal seperated:"));
  
    for (byte i = 0; i < mfrc522.uid.size; i++) {
      //Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
      Serial.print(mfrc522.uid.uidByte[i], DEC);
      Serial.print(F(" "));
      //inStringDec += String(mfrc522.uid.uidByte[i], DEC);
    }
  
    inStringHex = "";
    inStringHexRev = "";
    Serial.println();
    Serial.println(F("Card UID:"));
    Serial.print(WorkingStringA);
    Serial.println();
    Serial.println(F("Card UID Reversed:"));
    Serial.print(WorkingStringB);

    SendCardID(WorkingStringB);
  }
}
//************send the Card UID to the website*************
void SendCardID(String Card_uid ){
  if(WiFi.isConnected()){
    HTTPClient http;    //Declare object of class HTTPClient
    
    WiFiClient wifiClient;
    
    postData =  "{\"rfid_number\":\"" + Card_uid + "\"}";
    Serial.println(postData);
    delay(100);
    http.begin(wifiClient, URL);
    http.addHeader("Content-Type", "application/json");    
    
    int httpCode = http.POST(postData);
    String payload = http.getString();    //Get the response payload

    Serial.println(httpCode);   //Print HTTP return code
    Serial.println(payload);    //Print request response payload

    if (httpCode == 200) {
      delay(100);
    }
  }
}

//********************connect to the WiFi******************
void connectToWiFi(){
  WiFi.mode(WIFI_OFF);        //Prevents reconnection issue (taking too long to connect)
  delay(1000);
  WiFi.mode(WIFI_STA);
  Serial.print("\n Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println("");
  Serial.println("Connected");

  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //IP address assigned to your ESP
  
  delay(1000);
}
//=======================================================================
